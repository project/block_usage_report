<?php

namespace Drupal\block_usage_report\Controller;

use Drupal\block_usage_report\BlockContentPseudoEntity;
use Drupal\block_usage_report\LayoutBlockFinder;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilderInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Logger\LoggerChannel;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns output for the block usage report.
 */
class BlockUsageReportController extends ControllerBase {

  /**
   * Constructor for this controller.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $displayRepository
   *   The entity display-mode repository service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $blockStorage
   *   The block storage provider.
   * @param \Drupal\Core\Entity\EntityStorageInterface $blockContentStorage
   *   The block content storage provider.
   * @param \Drupal\Core\Entity\EntityListBuilderInterface $blockListBuilder
   *   The block list builder.
   * @param \Drupal\Core\Entity\EntityListBuilderInterface $blockContentListBuilder
   *   The block content list builder.
   * @param \Drupal\Core\Entity\EntityStorageInterface|null $fixedBlockContentStorage
   *   The fixed block content storage provider, or NULL if the associated
   *   module does not exist.
   * @param bool $hasBlockField
   *   TRUE if the block_field module is present and enabled, FALSE otherwise.
   * @param \Drupal\block_usage_report\LayoutBlockFinder|null $layoutBlockFinder
   *   The layout block finder service, if layout_builder is enabled.
   */
  public function __construct(
    protected EntityTypeBundleInfoInterface $bundleInfo,
    protected EntityDisplayRepositoryInterface $displayRepository,
    protected EntityStorageInterface $blockStorage,
    protected EntityStorageInterface $blockContentStorage,
    protected EntityListBuilderInterface $blockListBuilder,
    protected EntityListBuilderInterface $blockContentListBuilder,
    protected ?EntityStorageInterface $fixedBlockContentStorage,
    protected BlockManagerInterface $blockManager,
    protected Connection $connection,
    protected LoggerChannel $logger,
    protected bool $hasBlockField,
    protected ?LayoutBlockFinder $layoutBlockFinder,
  ) { }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $module_handler */
    $module_handler = $container->get('module_handler');
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_mgr */
    $entity_type_mgr = $container->get('entity_type.manager');
    /** @var \Drupal\Core\Database\Connection $connection */
    $connection = $container->get('database');
    /** @var \Drupal\Core\Block\BlockManagerInterface $block_mgr */
    $block_mgr = $container->get('plugin.manager.block');
    /** @var \Drupal\Core\Logger\LoggerChannel $logger */
    $logger = $container->get('logger.channel.block_usage_report');
    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info */
    $bundle_info = $container->get('entity_type.bundle.info');
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repo */
    $entity_display_repo = $container->get('entity_display.repository');

    $fixed_block_storage = NULL;
    if (
      $module_handler->moduleExists('fixed_block_content')
      && $entity_type_mgr->hasHandler('fixed_block_content', 'storage')
    ) {
      $fixed_block_storage = $entity_type_mgr->getStorage('fixed_block_content');
    }

    $has_block_field = $module_handler->moduleExists('block_field');
    if ($module_handler->moduleExists('layout_builder')) {
      $layout_block_finder = $container->get('layout_block_finder');
    }
    else {
      $layout_block_finder = NULL;
    }

    return new static(
      $bundle_info,
      $entity_display_repo,
      $entity_type_mgr->getStorage('block'),
      $entity_type_mgr->getStorage('block_content'),
      $entity_type_mgr->getListBuilder('block'),
      $entity_type_mgr->getListBuilder('block_content'),
      $fixed_block_storage,
      $block_mgr,
      $connection,
      $logger,
      $has_block_field,
      $layout_block_finder,
    );
  }

  /**
   * Finds a content entity referenced by a UUID, given a collection.
   *
   * @param string $uuid
   *   The UUID to search for.
   * @param \Drupal\block_usage_report\BlockContentPseudoEntity[] $entities
   *   The collection of entities to search for.
   *
   * @return \Drupal\block_usage_report\BlockContentPseudoEntity|null
   *   THe matching entity, or NULL if not found.
   */
  protected function getEntityByUuid(string $uuid, array $entities): ?BlockContentPseudoEntity {
    foreach ($entities as $entity) {
      if ($entity->uuid() == $uuid) {
        return $entity;
      }
    }
    return NULL;
  }

  /**
   * Builds the output of the report.
   *
   * @return array
   *   The render-array for the report.
   */
  public function build(): array {
    $bc_blocks = BlockContentPseudoEntity::loadMultiple();
    $bc_uuids_found = [];

    /** @var \Drupal\block\Entity\Block[] $blocks */
    $blocks = $this->blockStorage->loadMultiple();
    $public_theme_name = $this->config('system.theme')->get('default');

    $header = [
      $this->t('Name'),
      $this->t('Provider'),
      $this->t('Type'),
      $this->t('Region'),
      $this->t('Operations'),
    ];

    $enabled_rows = $disabled_rows = [];

    foreach ($blocks as $block) {
      $block_theme = $block->getTheme();
      if ($block_theme != $public_theme_name) {
        continue;
      }
      $settings = $block->get('settings');
      $name = $block->label() . ' (' . $block->id() . ')';
      $provider = $settings['provider'];
      $region = $block->getRegion();
      $status = $block->status();
      $plugin_id = $block->getPluginId();
      if (str_contains($plugin_id, ':')) {
        $type = explode(':', $plugin_id)[1];
      }
      else {
        $type = '';
      }

      if ($provider == 'block_content') {
        $uuid = explode(':', $plugin_id)[1];
        $block_content = $this->getEntityByUuid($uuid, $bc_blocks);
        $type = $block_content ? $block_content->bundle() : $this->t('[broken/missing]');
        $bc_uuids_found[] = $uuid;
      }
      elseif (
        $provider == 'fixed_block_content'
        && !empty($type)
        && !empty($this->fixedBlockContentStorage)
        /** @var \Drupal\fixed_block_content\Entity\FixedBlockContent $fixed_block */
        && $fixed_block = $this->fixedBlockContentStorage->load($type)
      ) {
        // Register the fixed block's associated custom block as in-use.
        /** @var \Drupal\block_content\BlockContentInterface $block_content */
        $block_content = $fixed_block->getBlockContent();
        // Show the user the bundle and ID of the associated custom block.
        $type .= '-' . $block_content->bundle() . '-' . $block_content->id();
        $bc_uuids_found[] = $block_content->uuid();
      }

      $row = [
        $name,
        $provider,
        $type,
        $region,
        [
          'data' => [
            '#type' => 'operations',
            '#links' => $this->blockListBuilder->getOperations($block),
          ],
        ],
      ];

      if ($status) {
        $enabled_rows[] = $row;
      }
      else {
        $disabled_rows[] = $row;
      }

    }

    // Sort first by provider, then by type, then by name.
    $sorter = function ($a, $b) {
      if ($a[1] != $b[1]) {
        return $a[1] <=> $b[1];
      }
      if ($a[2] != $b[2]) {
        return $a[2] <=> $b[2];
      }
      return $a[0] <=> $b[0];
    };

    usort($enabled_rows, $sorter);
    usort($disabled_rows, $sorter);

    $fielded_blocks = $this->getFieldedBlockInfo();
    foreach (array_keys($fielded_blocks) as $plugin_id) {
      if (str_starts_with('block_content:', $plugin_id)) {
        $bc_uuids_found[] = substr($plugin_id, 14);
      }
    }

    $layout_default_block_rows = [];
    if ($this->layoutBlockFinder) {
      $data = $this->layoutBlockFinder->getDefaultLayoutBlocks();
      if (!empty($data)) {
        foreach ($data as $entity_type_id => $subdata) {
          $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
          $entity_type_label = $entity_type_id == 'node' ? $this->t('Node') : $entity_type->getLabel();
          $bundle_info = $this->bundleInfo->getBundleInfo($entity_type_id);
          foreach ($subdata as $bundle => $subsubdata) {
            $bundle_label = $bundle_info[$bundle]['label'];
            $display_modes = $this->displayRepository->getViewModeOptionsByBundle($entity_type_id, $bundle);
            foreach ($subsubdata as $view_mode => $plugin_list) {
              $display_mode_label = (string) $display_modes[$view_mode];
              foreach ($plugin_list as $plugin_id) {
                $layout_default_block_rows[] = [
                  $entity_type_label,
                  $bundle_label,
                  $display_mode_label,
                  $plugin_id,
                ];
                if (
                  str_starts_with('block_content:', $plugin_id)
                  && $id = explode(':', $plugin_id)[1]
                ) {
                  $bc_uuids_found[] = $id;
                }
                elseif (
                  str_starts_with('fixed_block_content:', $plugin_id)
                  && !empty($id = explode(':', $plugin_id)[1])
                  && !empty($this->fixedBlockContentStorage)
                  /** @var \Drupal\fixed_block_content\Entity\FixedBlockContent $fixed_block */
                  && $fixed_block = $this->fixedBlockContentStorage->load($id)
                ) {
                  $bc_uuids_found[] = $fixed_block->getBlockContent()->uuid();
                }
              }
            }
          }
        }
      }
    }

    $layout_override_block_rows = [];
    if ($this->layoutBlockFinder) {
      $data = $this->layoutBlockFinder->getOverriddenLayoutBlocks();
      foreach ($data as $entity_type_id => $subdata) {
        $entity_type = $this->entityTypeManager()->getDefinition($entity_type_id);
        $entity_type_label = $entity_type_id == 'node' ? $this->t('Node') : $entity_type->label();
        $entity_storage = $this->entityTypeManager()->getStorage($entity_type_id);
        foreach ($subdata as $entity_id => $subsubdata) {
          foreach ($subsubdata as $langcode => $plugin_list) {
            /** @var \Drupal\Core\Entity\ContentEntityInterface[] $entities */
            $entities = $entity_storage->loadByProperties([
              $entity_type->getKey('id') => $entity_id,
              $entity_type->getKey('langcode') => $langcode,
            ]);
            $entity = reset($entities);
            foreach ($plugin_list as $plugin_id) {
              if ($entity_type->hasLinkTemplate('edit-form')) {
                $link = ['data' => $entity->toLink(NULL, 'edit-form')->toRenderable()];
              }
              elseif ($entity_type->hasLinkTemplate('canonical')) {
                $link = ['data' => $entity->toLink()->toRenderable()];
              }
              else {
                $link = $entity->label();
              }
              $layout_override_block_rows[] = [
                $entity_type_label,
                $langcode,
                $link,
                $plugin_id,
              ];
              if (
                str_starts_with('block_content:', $plugin_id)
                && !empty($id = explode(':', $plugin_id)[1])
              ) {
                $bc_uuids_found[] = $id;
              }
              elseif (
                str_starts_with('fixed_block_content:', $plugin_id)
                && !empty($id = explode(':', $plugin_id)[1])
                && !empty($this->fixedBlockContentStorage)
                /** @var \Drupal\fixed_block_content\Entity\FixedBlockContent $fixed_block */
                && $fixed_block = $this->fixedBlockContentStorage->load($id)
              ) {
                $bc_uuids_found[] = $fixed_block->getBlockContent()->uuid();
              }
            }
            unset($entity);
          }
        }
      }
    }

    $unplaced_blocks = [];
    foreach ($bc_blocks as $bc_block) {
      if (!in_array($bc_block->uuid(), $bc_uuids_found)) {
        $bc_block_entity = $this->blockContentStorage->load($bc_block->id());
        $unplaced_blocks[] = [
          $bc_block->label() . ' (' . $bc_block->id() . ')',
          $bc_block->bundle(),
          [
            'data' => [
              '#type' => 'operations',
              '#links' => $this->blockContentListBuilder->getOperations($bc_block_entity),
            ],
          ],
        ];
        // Allow garbage-collection to do its thing.
        unset($bc_block_entity);
      }
    }

    if (!empty($unplaced_blocks)) {
      // Sort first by type, then by name.
      usort($unplaced_blocks, function ($a, $b) {
        if ($a[1] != $b[1]) {
          return $a[1] <=> $b[1];
        }
        return $a[0] <=> $b[0];
      });
    }

    $out = [];
    if (!empty($enabled_rows)) {
      $out['enabled_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('Enabled blocks (@ct)', ['@ct' => count($enabled_rows)]),
        '#description' => $this->t('Blocks enabled in regions or field instances.'),
        '#open' => FALSE,
      ];
      $out['enabled_wrapper']['enabled'] = [
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $enabled_rows,
      ];
    }
    if (!empty($disabled_rows)) {
      $out['disabled_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('Disabled blocks (@ct)', ['@ct' => count($disabled_rows)]),
        '#open' => FALSE,
        '#description' => $this->t('Blocks placed but not enabled in a region.'),
      ];
      $out['disabled_wrapper']['disabled'] = [
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $disabled_rows,
      ];
    }

    if (!empty($fielded_blocks)) {
      $out['fielded_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('Blocks embedded in content (@ct)', ['@ct' => count($fielded_blocks)]),
        '#open' => FALSE,
        '#description' => $this->t('Blocks embedded in content entities via the <code>@module</code> module.', ['@module' => 'block_field'])
      ];
      $out['fielded_wrapper']['fielded'] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('Name'),
          $this->t('Type'),
          $this->t('Parent Entity Label'),
          $this->t('Entity Type/Bundle'),
          $this->t('Operations'),
        ],
        '#rows' => $fielded_blocks,
      ];
    }

    if (!empty($layout_default_block_rows)) {
      $out['layout_default_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('Blocks in bundle-default layout templates (@ct)', ['@ct' => count($layout_default_block_rows)]),
        '#open' => FALSE,
        '#description' => $this->t('Blocks embedded in content entity per-bundle layouts via the <code>@module</code> module.', ['@module' => 'layout_builder']),
      ];
      $out['layout_default_wrapper']['default'] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('Entity type'),
          $this->t('Bundle'),
          $this->t('Display mode'),
          $this->t('Plugin ID'),
        ],
        '#rows' => $layout_default_block_rows,
      ];
    }

    if (!empty($layout_override_block_rows)) {
      $out['layout_override_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('Blocks in entity-specific overridden layout templates (@ct)', ['@ct' => count($layout_override_block_rows)]),
        '#open' => FALSE,
        '#description' => $this->t('Blocks embedded in custom overridden layouts of specific entities via the <code>@module</code> module.', ['@module' => 'layout_builder']),
      ];
      $out['layout_override_wrapper']['override'] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('Entity type'),
          $this->t('Language code'),
          $this->t('Entity title'),
          $this->t('Plugin ID'),
        ],
        '#rows' => $layout_override_block_rows,
      ];
    }

    if (!empty($unplaced_blocks)) {
      $out['unplaced_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('Unplaced custom blocks (@ct)', ['@ct' => count($unplaced_blocks)]),
        '#open' => FALSE,
        '#description' => $this->t('Custom blocks not placed in any region or field instance.'),
      ];
      $out['unplaced_wrapper']['unplaced'] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('Name'),
          $this->t('Type'),
          $this->t('Operations'),
        ],
        '#rows' => $unplaced_blocks,
      ];
    }
    return $out;
  }

  /**
   * Returns information about all blocks embedded in block_field instances.
   *
   * @return array[]
   *   Info formatted as rows for table theming.
   */
  protected function getFieldedBlockInfo(): array {
    // Bail if this module is absent.
    if (!$this->hasBlockField) {
      return [];
    }

    // Store our gathered return values here.
    $block_field_info = [];

    // Find all instances of block_field fields.
    /** @var \Drupal\field\Entity\FieldStorageConfig[] $block_fields */
    $block_fields = $this->entityTypeManager()
      ->getStorage('field_storage_config')
      ->loadByProperties(['type' => 'block_field']);

    foreach ($block_fields as $block_field) {
      $field_name = $block_field->getName();
      $entity_type = $block_field->getTargetEntityTypeId();

      // Look up individual instantiations of this field.
      $db_rows = $this->connection->select($entity_type . '__' . $field_name, 'f')
        ->fields('f', ['entity_id', $field_name . '_plugin_id'])
        ->execute()
        ->fetchAll(\PDO::FETCH_NUM);
      foreach ($db_rows as $db_row) {
        [$entity_id, $plugin_id] = $db_row;
        $parent_entity = $this->getOwningEntity($entity_type, $entity_id);
        if (empty($parent_entity) || array_key_exists($plugin_id, $block_field_info)) {
          // Orphaned or malformed, or already found. Skip this one.
          continue;
        }
        if ($parent_entity instanceof EntityPublishedInterface && !$parent_entity->isPublished()) {
          // Omit fielded blocks whose parent entity is unpublished.
          // @todo Revisit this design choice.
          continue;
        }

        $block_def = $this->blockManager->getDefinition($plugin_id);
        // Operations are shown for the owning content entity, NOT the block
        // itself.
        $ops = $this->entityTypeManager()
          ->getListBuilder($parent_entity->getEntityTypeId())
          ->getOperations($parent_entity);

        $block_field_info[$plugin_id] = [
          $block_def['admin_label'] . ' (' . $plugin_id . ')',
          $block_def['provider'],
          ['data' => $parent_entity->toLink()->toRenderable()],
          $parent_entity->getEntityType()->getLabel() . ': ' . $parent_entity->bundle(),
          [
            'data' => [
              '#type' => 'operations',
              '#links' => $ops,
            ],
          ],
        ];
      }
    }

    return $block_field_info;
  }

  /**
   * Finds and loads an entity, escalating up the ownership tree for paragraphs.
   *
   * @param string $entity_type
   *   The entity-type of the current iteration.
   * @param int $entity_id
   *   The entity ID of the current iteration.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The content entity, or NULL in the case of orphaned entities.
   */
  protected function getOwningEntity(string $entity_type, int $entity_id): ?EntityInterface {
    if ($entity_type == 'paragraph') {
      try {
        $parent_data = $this->connection->select('paragraphs_item_field_data', 'p')
          ->fields('p', ['parent_id', 'parent_type'])
          ->condition('id', $entity_id)
          ->execute()
          ->fetch(\PDO::FETCH_NUM);
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
        $parent_data = NULL;
      }
      if (empty($parent_data)) {
        // This is an orphaned paragraph; ignore.
        $this->logger->warning('Paragraph %d has no valid parent entity reference.', ['%d' => $entity_id]);
        return NULL;
      }
      [$entity_id, $entity_type] = $parent_data;
      if ($entity_type == 'paragraph') {
        return $this->getOwningEntity($entity_type, $entity_id);
      }
    }
    try {
      return $this->entityTypeManager()
        ->getStorage($entity_type)
        ->load($entity_id);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return NULL;
    }
  }

}
