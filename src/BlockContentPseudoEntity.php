<?php

namespace Drupal\block_usage_report;

/**
 * A more lightweight stub to substitute for a full BlockContent entity.
 */
class BlockContentPseudoEntity {

  /**
   * Class constructor. Should never need to be externally instantiated.
   *
   * Note that all arguments are promoted to class properties.
   *
   * @param string $uuid
   *   The block-content UUID.
   * @param string $type
   *   The bundle of the block-content entity.
   * @param string $info
   *   The block-content label.
   * @param int $id
   *   The ID of the block-content entity.
   */
  protected function __construct(
    protected string $uuid,
    protected string $type,
    protected string $info,
    protected int $id
  ) {
    // Nothing here.
  }

  /**
   * Loads and returns info for all block-content entities.
   *
   * @return static[]
   */
  public static function loadMultiple(): array {
    $connection = \Drupal::database();
    $query = $connection->select('block_content', 'bc');
    $query->innerJoin('block_content_field_data', 'bcfd', 'bc.id = bcfd.id');
    $result = $query->fields('bc', ['uuid'])
      ->fields('bcfd', ['id', 'type', 'info'])
      ->execute();
    $blocks = [];
    foreach ($result as $row) {
      $blocks[] = new static(
        $row->uuid,
        $row->type,
        $row->info,
        $row->id
      );
    }
    return $blocks;
  }

  /**
   * Returns the UUID.
   *
   * @return string
   */
  public function uuid(): string {
    return $this->uuid;
  }

  /**
   * Returns the bundle.
   *
   * @return string
   */
  public function bundle(): string {
    return $this->type;
  }

  /**
   * Returns the label.
   *
   * @return string
   */
  public function label(): string {
    return $this->info;
  }

  /**
   * Returns the numeric ID.
   *
   * @return int
   */
  public function id(): int {
    return $this->id;
  }

}