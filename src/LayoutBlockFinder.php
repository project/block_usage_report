<?php

declare(strict_types=1);

namespace Drupal\block_usage_report;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\layout_builder\Section;

/**
 * Service to find usages of blocks by layout_builder.
 */
class LayoutBlockFinder {

  /**
   * IDs of block plugins that exist in entity-type/bundle default layouts.
   *
   * @var array
   */
  protected array $defaultLayoutBlocks;

  /**
   * Service class constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   */
  public function __construct(
    protected Connection $connection,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) { }

  /**
   * Fetches blocks that are used by the default layout per bundle.
   *
   * @return array
   *   A multi-level array, grouped by entity-type, bundle, view-mode, and list
   *   of block plugin-ids.
   */
  public function getDefaultLayoutBlocks(): array {
    if (!isset($this->defaultLayoutBlocks)) {
      $this->defaultLayoutBlocks = [];
      $configs = $this->connection->select('config', 'c')
        ->fields('c', ['name', 'data'])
        ->condition('name', 'core.entity_view_display.%', 'LIKE')
        ->condition('data', '%layout_builder%', 'LIKE')
        ->execute()
        ->fetchAllKeyed();
      foreach ($configs as $name => $config) {
        [,, $entity_type_id, $bundle, $view_mode] = explode('.', $name);
        $config = unserialize($config);
        // Skip if layout builder is not enabled.
        if (!($config['third_party_settings']['layout_builder']['enabled'] ?? FALSE)) {
          continue;
        }
        foreach ($config['third_party_settings']['layout_builder']['sections'] as $section) {
          foreach ($section['components'] as $component) {
            $plugin_id = $component['configuration']['id'] ?? NULL;
            // Ignore field blocks.
            if (
              !empty($plugin_id)
              && !str_starts_with($plugin_id, 'field_block:')
              && !str_starts_with($plugin_id, 'extra_field_block:')
            ) {
              $this->defaultLayoutBlocks[$entity_type_id][$bundle][$view_mode][] = $plugin_id;
            }
          }
        }
      }
    }
    return $this->defaultLayoutBlocks;
  }

  /**
   * Fetches blocks that are overrides of default layouts.
   *
   * @return array
   *   Multi-level array consisting of entity-type, id, langcode, and list of
   *   overridden block plugin-ids.
   */
  public function getOverriddenLayoutBlocks(): array {
    $items = [];
    $entity_types = $this->entityTypeManager->getDefinitions();
    foreach ($entity_types as $entity_type) {
      // Only interested in content entities.
      if (!$entity_type instanceof ContentEntityTypeInterface) {
        continue;
      }
      $entity_type_id = $entity_type->id();
      $table = "{$entity_type_id}__layout_builder__layout";
      if ($this->connection->schema()->tableExists($table)) {
        $result = $this->connection->select($table, 't')
          ->fields('t', ['entity_id', 'bundle', 'langcode', 'layout_builder__layout_section'])
          ->condition('deleted', 0)
          ->orderBy('entity_id')
          ->orderBy('delta')
          ->execute();
        foreach ($result as $row) {
          /** @var \Drupal\layout_builder\Section $section */
          $section = @unserialize($row->layout_builder__layout_section);
          if (!$section instanceof Section) {
            continue;
          }
          foreach ($section->getComponents() as $component) {
            $plugin_id = $component->getPluginId();
            // Ignore field_blocks and any blocks that are in the entity
            // default layout.
            if (
              str_starts_with($plugin_id, 'field_block:')
              || str_starts_with($plugin_id, 'extra_field_block:')
              || !$this->isOverride($entity_type_id, $row->bundle, $plugin_id)
            ) {
              continue;
            }
            $items[$entity_type_id][$row->entity_id][$row->langcode][] = $plugin_id;
          }
        }
      }
    }
    return $items;
  }

  /**
   * Checks to see if a placed block is an override of the default layout.
   *
   * @param string $entity_type_id
   *   The entity type id to be checked.
   * @param string $block_plugin_id
   *   The block plugin ID to be searched for.
   *
   * @return bool
   *   TRUE if this is an override, FALSE if the same block exists in the
   *   default layout for this
   */
  protected function isOverride(string $entity_type_id, string $bundle, string $block_plugin_id): bool {
    if (!is_array($this->defaultLayoutBlocks)) {
      $this->getDefaultLayoutBlocks();
    }
    if (!isset($this->defaultLayoutBlocks[$entity_type_id][$bundle])) {
      // If there are no default blocks for this bundle, any block here is an
      // override.
      return TRUE;
    }
    foreach ($this->defaultLayoutBlocks[$entity_type_id][$bundle] as $data) {
      if (in_array($block_plugin_id, $data)) {
        return FALSE;
      }
    }
    return TRUE;
  }

}
